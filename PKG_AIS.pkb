CREATE OR REPLACE PACKAGE BODY FOND.PKG_AIS AS
/******************************************************************************
   NAME:       PKG_AIS
   PURPOSE:

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        20/02/2020      lev_sv       1. Created this package body.
******************************************************************************/

--------------------------------------------------------------------------------
-- ������ ���-�������
--------------------------------------------------------------------------------

-- 1) �������� ��������� ��������� ��� �������
PROCEDURE GET_RST_TO_AIS(
    REFILL#     IN INT,     -- ��������������� ������ �� �������� ������
    YEAR#       IN INT,     -- �������� ���
    MONTH#      IN INT,     -- �������� �����
    NPP_MIN#    IN INT,     -- ������� � ������ �����
    NPP_MAX#    IN INT,     -- ���������� ������� �����
    RET$        OUT TYPES.RetCursor -- ������
)
IS
    ID_PERIOD#  INT;
    X           INT:=0;
BEGIN

    BEGIN
    SELECT PERIOD.ID INTO ID_PERIOD#
        FROM F2014.PERIOD
        WHERE YEAR = YEAR#
        AND MONTH = MONTH#;
    EXCEPTION
    WHEN NO_DATA_FOUND THEN ID_PERIOD#:=NULL;
    WHEN OTHERS THEN RAISE;
    END;
    
    BEGIN
    SELECT 1 INTO X
        FROM DUAL
        WHERE EXISTS (
            SELECT *
                FROM AIS_MODELS
                WHERE ID_PERIOD = ID_PERIOD#
        );
    EXCEPTION
    WHEN NO_DATA_FOUND THEN X:=0;
    WHEN OTHERS THEN RAISE;
    END;
    
--    TIME_MARK#:=LOCALTIMESTAMP;
    
--    WITH T AS (
--        SELECT SYSTIMESTAMP - TO_TIMESTAMP('01-01-1970','dd-mm-yyyy' ) DIFF 
--            FROM DUAL
--        )
--        SELECT 
--            EXTRACT(DAY FROM DIFF) * 24 * 3600000+
--            EXTRACT(HOUR FROM DIFF) * 3600000+
--            EXTRACT(MINUTE FROM DIFF) * 60000 +
--            EXTRACT(SECOND FROM DIFF) * 1000 +
--            TO_NUMBER(REGEXP_SUBSTR(TO_CHAR(DIFF,'SSFF3'),'\d+$')) DIF INTO TIME_MARK#
--        FROM T;

    IF X = 0 OR NVL(REFILL#,0) = 1 THEN
    
        DELETE FROM AIS_MODELS WHERE ID_PERIOD = ID_PERIOD#;
    
        INSERT INTO FOND.AIS_MODELS (
            ID_PERIOD, GKEY, IDSL, 
            TIME_MARK, OT_PER, MSK_OT, 
            CODE_MSK, VID_MP, USL_OK, 
            PROFIL, MKB1, MKB2, 
            MKB3, CODE_MES, CODE_USL, 
            CODE_MD, KOL_USL, KOL_FACT, 
            ISH_MOV, RES_GOSP, TARIF_B, 
            TARIF_S, TARIF_1K, SUM_RUB, 
            VID_TR, EXTR, CODE_OTD, 
            SOUF, SPEC_MD, DOMC_TYPE, 
            OKATO_INS, NOVOR, CODE_LPU, 
            VID_SF, NHISTORY, PERSCODE, 
            DATE_IN, DATE_OUT, TARIF_D, 
            VID_KOEFF, USL_TMP, BIRTHDAY, 
            SEX, COUNTRY, SEX_P, 
            BIRTHDAY_P, INV, DATE_NPR, 
            FOR_POM, MSE, P_CEL, 
            DN, TAL_P, PROFIL_K, 
            NAPR_MO, MKB0, DS_ONK, 
            VAL_KOEFF, C_ZAB, MEE, 
            EKMP, NPP) 
            SELECT /*+ INDEX(USL IDX__USL_SLUCH) INDEX(PACIENT PACIENT__PK) INDEX(SCHET SCHET_SMO_PK) INDEX(PERS IDX_PERS_PK) INDEX(KSG IDX_KSG_SLUCH)*/
                SLUCH.ID_PERIOD,
                TRIM(TO_CHAR(USL.IDSERV)) AS GKEY,
                TRIM(TO_CHAR(SLUCH.IDCASE)) AS IDSL,
                --TIME_MARK# AS TIME_MARK,
                DATE_TO_INT(NULL) AS TIME_MARK,
                TRIM(TO_CHAR(SCHET.MONTH, '09'))||TRIM(TO_CHAR(SCHET.YEAR, '9999')) AS OT_PER,
                NVL(PACIENT.SMO, PACIENT.ST_OKATO) AS MSK_OT,
                SMO_OGRN AS CODE_MSK,
                TRIM(TO_CHAR(SLUCH.VIDPOM)) VID_MP,
                TO_CHAR(SLUCH.USL_OK) AS USL_OK, 
                TO_CHAR(USL.PROFIL) AS PROFIL,
                SLUCH.DS1 AS MKB1,
                (SELECT REPLACE(WM_CONCAT(T.DS),',',';') FROM F2014.DS T WHERE T.ID_SLUCH = SLUCH.ID AND T.TYPE = 2) AS MKB2,
                (SELECT REPLACE(WM_CONCAT(T.DS),',',';') FROM F2014.DS T WHERE T.ID_SLUCH = SLUCH.ID AND T.TYPE = 3) AS MKB3,
                SLUCH.CODE_MES1 AS CODE_MES,
                USL.CODE_USL,
                USL.CODE_MD,
                USL.KOL_USL AS KOL_USL,
                (CASE WHEN SLUCH.USL_OK IN (1,2) AND USL.KOD_EU IN (1,2) THEN USL.KOL_EU ELSE NULL END) AS KOL_FACT,
                TO_CHAR(SLUCH.ISHOD) AS ISH_MOV,
                TO_CHAR(SLUCH.RSLT) AS RES_GOSP,
                NVL(ROUND(KSG.BZTSZ * KSG.KOEF_Z, 2), -1) AS TARIF_B,
                USL.TARIF AS TARIF_S,
                0 AS TARIF_1K,
                USL.SUMV_USL AS SUM_RUB,
                NULL AS VID_TR,
                NULL AS EXTR,
                TO_CHAR(USL.PODR) AS CODE_OTD,
                1 AS SOUF,
                TO_CHAR(USL.PRVS) AS SPEC_MD,
                TO_CHAR(PACIENT.VPOLIS) AS DOMC_TYPE,
                NVL(SMO_OK, ST_OKATO) AS OKATO_INS,
                PACIENT.NOVOR,
                SCHET.CODE_MO AS CODE_LPU,
                NULL AS VID_SF,
                SLUCH.NHISTORY,
                TO_CHAR(SLUCH.PID) AS PERSCODE,
                DATE_TO_INT(USL.DATE_IN) AS DATE_IN,
                DATE_TO_INT(USL.DATE_OUT) AS DATE_OUT,
                0 AS TARIF_D,
                NULL AS VID_KOEFF,
                NULL AS USL_TMP,
                DATE_TO_INT(PERS.DR) AS BIRTHDAY,
                TO_CHAR(PERS.W) AS SEX,
                (SELECT T.CN FROM PEOPLE T WHERE T.ID = SLUCH.PID) AS COUNTRY,
                TO_CHAR(PERS.W_P) AS SEX_P,
                NVL2(PERS.DR_P, DATE_TO_INT(PERS.DR_P), NULL) AS BIRTHDAY_P,
                PACIENT.INV,
                NVL2(SLUCH.NPR_DATE, DATE_TO_INT(SLUCH.NPR_DATE), NULL) AS DATE_NPR,
                SLUCH.FOR_POM AS FOR_POM,
                PACIENT.MSE,
                SLUCH.P_CEL,
                NVL(SLUCH.DN,0) AS DN,
                NVL2(SLUCH.TAL_P, DATE_TO_INT(SLUCH.TAL_P), NULL) AS TAL_P,
                NVL(SLUCH.PROFIL_K,0) AS PROFIL_K,
                (SELECT T.NAPR_MO FROM F2014.USL_NAPR T WHERE T.ID_SLUCH = SLUCH.ID AND ROWNUM=1) AS NAPR_MO,
                SLUCH.DS0 AS MKB0,
                SLUCH.DS_ONK,
                NVL(KSG.IT_SL,1) * NVL(KSG.KOEF_U,1) * NVL(KSG.KOEF_UP,1) * NVL(KSG.KOEF_SK,1) AS VAL_KOEFF,
                SLUCH.C_ZAB,
                NVL((SELECT
                    (CASE 
                        WHEN SUM(T.S_SUM_DELTA) > 0 THEN 1 
                        WHEN COUNT(*) > 0 AND NVL(SUM(T.S_SUM_DELTA),0) = 0 THEN 0  
                        ELSE NULL 
                    END) 
                    FROM F2014.BACK_SANK T 
                    WHERE T.ID_REESTR = SLUCH.ID_REESTR 
                    AND T.S_TIP IN (SELECT S.IDVID FROM SPR.SPFF_F006 S WHERE S.S_TIP = 2)), -1) AS MEE, 
                NVL((SELECT 
                    (CASE 
                        WHEN SUM(T.S_SUM_DELTA) > 0 THEN 1 
                        WHEN COUNT(*) > 0 AND NVL(SUM(T.S_SUM_DELTA),0) = 0 THEN 0  
                        ELSE NULL 
                    END) 
                    FROM F2014.BACK_SANK T 
                    WHERE T.ID_REESTR = SLUCH.ID_REESTR 
                    AND T.S_TIP IN (SELECT S.IDVID FROM SPR.SPFF_F006 S WHERE S.S_TIP = 3)),-1) AS EKMP,
                    ROWNUM AS NPP     
            --
                FROM F2014.SLUCH SLUCH 
                JOIN F2014.PACIENT PACIENT ON PACIENT.ID = SLUCH.ID_PACIENT AND NVL(PACIENT.PR_NOV,0) = 0
                JOIN F2014.PERS PERS ON PACIENT.ID_PERS = PERS.ID AND PACIENT.ID_SCHET = PERS.ID_SCHET
                JOIN F2014.USL USL ON USL.ID_SLUCH = SLUCH.ID AND NVL(USL.PR_NOV,0) = 0
                JOIN F2014.SCHET_SMO SCHET ON SLUCH.ID_SCHET = SCHET.ID
                LEFT JOIN F2014.KSG KSG ON SLUCH.ID = KSG.ID_SLUCH
                WHERE SLUCH.ID_PERIOD = ID_PERIOD#
--                AND SLUCH.LPU = NVL(MKOD#,SLUCH.LPU)
                AND SLUCH.ERROR IS NULL;
            
        COMMIT;
                
    END IF;
    
    OPEN RET$ FOR
        SELECT GKEY, IDSL, 
            GKEY, IDSL, 
            TIME_MARK, OT_PER, MSK_OT, 
            CODE_MSK, VID_MP, USL_OK, 
            PROFIL, MKB1, MKB2, 
            MKB3, CODE_MES, CODE_USL, 
            CODE_MD, KOL_USL, KOL_FACT, 
            ISH_MOV, RES_GOSP, TARIF_B, 
            TARIF_S, TARIF_1K, SUM_RUB, 
            VID_TR, EXTR, CODE_OTD, 
            SOUF, SPEC_MD, DOMC_TYPE, 
            OKATO_INS, NOVOR, CODE_LPU, 
            VID_SF, NHISTORY, PERSCODE, 
            DATE_IN, DATE_OUT, TARIF_D, 
            VID_KOEFF, USL_TMP, BIRTHDAY, 
            SEX, COUNTRY, SEX_P, 
            BIRTHDAY_P, INV, DATE_NPR, 
            FOR_POM, MSE, P_CEL, 
            DN, TAL_P, PROFIL_K, 
            NAPR_MO, MKB0, DS_ONK, 
            VAL_KOEFF, C_ZAB, MEE, 
            EKMP
        FROM AIS_MODELS
        WHERE ID_PERIOD = ID_PERIOD#
        AND NPP BETWEEN NPP_MIN# AND NPP_MAX#
        ORDER BY NPP;
    
--    FOR R# IN (
--        SELECT TIME_MARK 
--            FROM AIS_MODELS 
--            WHERE ID_PERIOD = ID_PERIOD# 
--            AND ROWNUM <= REC_COUNT# 
--            ORDER BY TIME_MARK
--    ) LOOP
--        CRC#:= BIT_FUNCTIONS.BIT_XOR(CRC#, R#.TIME_MARK);
--        CNT#:= CNT# + 1;
--    END LOOP;
--    
--    CHECKSUM$:=CRC#;
    
EXCEPTION
    WHEN OTHERS THEN 
        MYERROR(SQLCODE,'PKG_AIS.GET_RST_TO_AIS> '||SQLERRM);   
        COMMIT;
END GET_RST_TO_AIS;

--FUNCTION GET_CRC(ID_PERIOD# IN INT)
--RETURN NUMBER
--IS
--    CRC# NUMBER:=0;
--BEGIN
--    FOR R# IN (SELECT TIME_MARK FROM AIS_MODELS WHERE ID_PERIOD = ID_PERIOD#) LOOP
--        
----        DBMS_OUTPUT.PUT_LINE('CRC='||TO_CHAR(CRC#||' TIME_MARK='||TO_CHAR(R#.TIME_MARK)));
--        
--        CRC#:= BIT_FUNCTIONS.BIT_XOR(CRC#, R#.TIME_MARK);
--        
--    END LOOP;
--    
--    DBMS_OUTPUT.PUT_LINE('PERIOD='||TO_CHAR(ID_PERIOD#)||' CRC='||TO_CHAR(CRC#));
--    
--    RETURN CRC#;
--END;

-- ����������� ����� ���� DATE � ����� ����� (���������� � 01.01.1970)
FUNCTION DATE_TO_INT(
    DATE#       IN DATE
)
RETURN INT 
IS
    tmpVar INT;
BEGIN
   tmpVar := 0;
   
   CASE 
   WHEN DATE# IS NULL THEN
       WITH T AS (
        SELECT LOCALTIMESTAMP - TO_TIMESTAMP('01-01-1970','dd-mm-yyyy' ) DIFF 
            FROM DUAL
        )
        SELECT 
            EXTRACT(DAY FROM DIFF) * 24 * 3600000 +
            EXTRACT(HOUR FROM DIFF) * 3600000 +
            EXTRACT(MINUTE FROM DIFF) * 60000 +
            EXTRACT(SECOND FROM DIFF) * 1000 +
            TO_NUMBER(REGEXP_SUBSTR(TO_CHAR(DIFF,'SSFF3'),'\d+$')) DIF 
            INTO tmpVar
        FROM T;
    ELSE
        WITH T AS (
        SELECT DATE# - TO_TIMESTAMP('01-01-1970','dd-mm-yyyy' ) DIFF 
            FROM DUAL
        )
        SELECT 
            EXTRACT(DAY FROM DIFF) * 24 * 3600 +
            EXTRACT(HOUR FROM DIFF) * 3600 +
            EXTRACT(MINUTE FROM DIFF) * 60 +
            EXTRACT(SECOND FROM DIFF) * 1 
            INTO tmpVar
        FROM T;
    END CASE;
   
   RETURN tmpVar;
   
   EXCEPTION
     WHEN NO_DATA_FOUND THEN
        tmpVar:=NULL;
     WHEN OTHERS THEN
       -- Consider logging the error and then re-raise
        RAISE;
END DATE_TO_INT;

PROCEDURE GET_CHECKSUM(
    YEAR#       IN INT,     -- �������� ���
    MONTH#      IN INT,     -- �������� �����
    NPP_MIN#    IN INT,     -- ������� � ������ �����
    NPP_MAX#    IN INT,     -- ���������� ������� �����
    CHECKSUM$   OUT INT    -- ����������� �����
) 
IS
    ID_PERIOD#  INT:=NULL;
    CRC#        INT:=0;
BEGIN

    BEGIN
    SELECT PERIOD.ID INTO ID_PERIOD#
        FROM F2014.PERIOD
        WHERE YEAR = YEAR#
        AND MONTH = MONTH#;
    EXCEPTION
    WHEN NO_DATA_FOUND THEN ID_PERIOD#:=NULL;
    WHEN OTHERS THEN RAISE;
    END;
    
    FOR R# IN (
        SELECT TIME_MARK 
            FROM AIS_MODELS 
            WHERE ID_PERIOD = ID_PERIOD# 
            AND NPP BETWEEN NPP_MIN# AND NPP_MAX#
            ORDER BY NPP
    ) LOOP
        CRC#:= BIT_FUNCTIONS.BIT_XOR(CRC#, R#.TIME_MARK);
    END LOOP;
    
    CHECKSUM$:=CRC#;
    
END;

PROCEDURE GET_RECCOUNT(
    YEAR#       IN INT,     -- �������� ���
    MONTH#      IN INT,     -- �������� �����
    RECCOUNT$   OUT INT    -- ���������� �������
)
IS
    ID_PERIOD#  INT:=NULL;
BEGIN

    BEGIN
    SELECT PERIOD.ID INTO ID_PERIOD#
        FROM F2014.PERIOD
        WHERE YEAR = YEAR#
        AND MONTH = MONTH#;
    EXCEPTION
    WHEN NO_DATA_FOUND THEN ID_PERIOD#:=NULL;
    WHEN OTHERS THEN RAISE;
    END;
    
    SELECT COUNT(NPP) INTO RECCOUNT$
        FROM AIS_MODELS 
        WHERE ID_PERIOD = ID_PERIOD#; 
END;

-- ����� ������ ��� ����������� � ����������� ������������������ ��������
PROCEDURE GET_RST_WITH_SCORE(
    MENU#       IN INT,             -- ��� ���� ������
    ID_USER#    IN INT,             -- ��� ������������ �� ������ (WEB_USERS.ID)
    ID_PERIOD#  IN INT,             -- ������������� ��������� ������� (F2014.PERIOD.ID)
    USL_OK#     IN INT,             -- ��� ������� �������� ������ (SPR.SPFF_V006.IDUMP)
    LPU#        IN VARCHAR2,        -- ��� �� (SPR.SP_LPU.MKOD) ����� �������� ��� ��� � �����, ���������� ��� ��. ������� ������� "��� ��"
    SMK#        IN VARCHAR2,        -- ��� ��� (SPR.SP_SMO.SMOCOD)  ����� �������� ��� �� � �����, ���������� ��� ���. ������� ������� "��� ���"
    LIST_DS#    IN VARCHAR2,        -- ������ ����� ���������, ����� ����������� (�������) �������� A16.1,O18.0 �������� ������������� �������
    TYPE_EXP#   IN INT,             -- ��� ����������: 2-���, 3-����
    SCORE_MIN#  IN INT,             -- ������ ������� ������. �� ��������� 80
    SCORE_MAX#  IN INT,             -- ������� ������� ������. �� ���������� 100
    RET_MSG$   OUT VARCHAR2,        -- ��������� ���������
    RET_CUR$   OUT TYPES.RETCURSOR  -- �������������� ������
) IS

    KOL_USERS#  INT:=0;
    SMO#        VARCHAR2(5):=NULL;
    MKOD#       VARCHAR2(6):=NULL;
    
BEGIN

    INSERT INTO FOND.EVENT_LOG (
        DATETIME, SQL_CODE, 
        SQL_ERROR, LOGIN, APPLICATION, 
        COMPUTER, VERSION) 
    VALUES ( 
        SYSDATE,
        NULL,
        'MENU='||TO_CHAR(NVL(MENU#,0))
        ||' ID_USER='||TO_CHAR(ID_USER#)
        ||' ID_PERIOD='||TO_CHAR(ID_PERIOD#)
        ||' USL_OK='||TO_CHAR(NVL(USL_OK#,0))
        ||' LPU='||NVL(LPU#,'NULL')
        ||' SMK='||NVL(SMK#,'NULL')
        ||' LIST_DS='||NVL(LIST_DS#,'NULL')
        ||' TYPE_EXP='||TO_CHAR(TYPE_EXP#)
        ||' SCORE_MIN='||TO_CHAR(SCORE_MIN#)
        ||' SCORE_MAX='||TO_CHAR(SCORE_MAX#)
        ,
        NULL,
        'AIS EXPERT',
        NULL,
        NULL);
        
    COMMIT;

    MKOD#:= LPU#;
    SMO#:=  SMK#;
    
    RET_MSG$:=NULL;

    FOR WEB_USER# IN (SELECT * FROM WEB_USERS T WHERE T.ID = ID_USER#) LOOP
    
        -- ���������� ��� ��� ��� ���������
        IF WEB_USER#.TYPE = 3 THEN
            SMO#:=WEB_USER#.FED_CODE;
        END IF;
        
        -- ��� �� ���������� ���, ����� ������������� ������������ � ���������� ������
        IF WEB_USER#.TYPE IN (1,4) THEN
            MKOD#:=WEB_USER#.FED_CODE;
        END IF;

        CASE 
        WHEN MENU# = 1 THEN 
            OPEN RET_CUR$ FOR 
                SELECT /*+ INDEX(SLUCH IDX_SLUCH_PERIOD) INDEX(LPU SP_LPU_UNIQ_MKOD) INDEX(SCORE IDX_AIS_SCORE_DATA_GKEY) */
                    DISTINCT
                    PP.FAM,                                     -- �������
                    PP.IM,                                      -- ���
                    PP.OT,                                      -- ��������
                    TO_CHAR(PP.DR, 'DD.MM.YYYY') AS DR,         -- ���� ��������
                    PP.ENP,                                     -- ���
                    PP.Q,                                       -- ��� ���
                    (SELECT S.SHORT_NAME FROM SPR.SP_SMO S WHERE S.SMOCOD = Q) Q_ABBR, -- ������� ������������ ���
                    SLUCH.LPU,                                  -- ��� ��
                    LPU.SHORT_NAME AS LPU_NAME,                 -- ��
                    SCHET.TYPE_PKG,                             -- ��� �����
                    (SELECT (CASE PACIENT.PR_NOV WHEN 1 THEN '��������' ELSE '�������' END) FROM F2014.PACIENT PACIENT WHERE SLUCH.ID_PACIENT = PACIENT.ID) AS PR_NOV,  -- ������� ���������� ����������
                    SLUCH.IDCASE,                               -- ���������� ������������� ������
                    (SELECT S.UMPSNAME FROM SPR.SPFF_V006 S WHERE S.IDUMP = SLUCH.USL_OK) AS USL_OK_NAME,   -- ������� ��������
                    TO_CHAR(SLUCH.DATE_1, 'DD.MM.YYYY') AS DATE_1,  -- ���� ������ �������
                    TO_CHAR(SLUCH.DATE_2, 'DD.MM.YYYY') AS DATE_2,  -- ���� ��������� �������
                    SLUCH.DS1, (SELECT S.NAME FROM SPR.SPFF_MKB10 S WHERE S.CODE = SLUCH.DS1) AS DS1_NAME,  -- ��� ��������
                    (SELECT S.NAME FROM SPR.SP_OWN S WHERE S.ID = SLUCH.OWN) AS OWN_NAME,                   -- ������ ��������������
                    SLUCH.SUMV,                                 -- �����������, ���
                    SLUCH.SUMP,                                 -- ��������, ���
                    SLUCH.SANK_IT,                              -- �������, ���
                    SCORE.MEE,                                  -- ����������� ������� ���
                    SCORE.EKMP                                  -- ����������� ������� ����
                    FROM F2014.SLUCH SLUCH
                    JOIN F2014.SCHET_SMO SCHET ON SLUCH.ID_SCHET = SCHET.ID AND SCHET.PLAT = NVL(SMO#, SCHET.PLAT)
                    JOIN F2014.USL USL ON SLUCH.ID = USL.ID_SLUCH
                    JOIN FOND.AIS_SCORE_DATA SCORE ON USL.IDSERV = SCORE.GKEY 
                        AND ((SCORE.MEE BETWEEN SCORE_MIN# AND SCORE_MAX# AND TYPE_EXP#=2) 
                            OR (SCORE.EKMP BETWEEN SCORE_MIN# AND SCORE_MAX#) AND SLUCH.ID_PERIOD = SCORE.ID_PERIOD AND TYPE_EXP# = 3)
                    JOIN FOND.PEOPLE PP ON SLUCH.PID = PP.ID
                    JOIN SPR.SP_LPU LPU ON LPU.MKOD = SLUCH.LPU
                    WHERE SLUCH.ID_PERIOD = ID_PERIOD#
                    AND SLUCH.LPU = NVL(MKOD#, SLUCH.LPU)
                    
                    AND SLUCH.ERROR IS NULL
                    AND SLUCH.USL_OK = NVL(USL_OK#, SLUCH.USL_OK)
                    AND (LIST_DS# LIKE '%SLUCH.DS1%' OR TRIM(LIST_DS#) IS NULL)
                    ORDER BY (CASE TYPE_EXP# WHEN 2 THEN SCORE.MEE WHEN 3 THEN SCORE.EKMP ELSE NULL END) DESC, FAM, IM, OT;
        ELSE
            NULL;
        END CASE;

    END LOOP;
    
    IF KOL_USERS# = 0 THEN
        RET_MSG$:='������������ �� ��������������� � �������';
        RETURN;
    END IF;
    
EXCEPTION
    WHEN NO_DATA_FOUND THEN
        NULL;
    WHEN OTHERS THEN
        MyERROR(SQLCODE,'PKG_AIS.GET_RST_WITH_SCORE> '||SUBSTR(SQLERRM, 1, 4000));
        COMMIT;
        RAISE;
END GET_RST_WITH_SCORE;

END PKG_AIS;
/
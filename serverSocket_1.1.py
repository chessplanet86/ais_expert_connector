# -*- coding: utf-8 -*-
"""
Главный файл для синхронизации с сервером и заливки 
"""
from inVal import *  # year, month, mkod
from commandAis import *
import socket
# Библиотека для подсветки текста в консоли
from colorama import init, Fore, Back, Style
import json, math
import oracleConnect as ora
import datetime
import helpFunction as hf

init()  # Инициализируем colorama
print(Fore.YELLOW+'Получаем данные из БД'+Style.RESET_ALL)
print(datetime.datetime.today().strftime('%H:%M:%S'))
# ******************* БЛОК ВВОДА ПАРАМЕТРОВ ******************
# *************************************************************
# ******************* КОМАНДЫ *********************************

# ************************************************************
lengthDataToAis = ora.getRecCount(year, month)
# ais = ora.getDataToAis(0, year, month, npp_min, npp_max)
crc = ora.getCheckSum(year, month, 1, lengthDataToAis)
print(crc)
# dataToAis = ais[:1]  # берем только первую запись
# dataToAis5000 = ais

maxCountPackages = math.ceil(lengthDataToAis/lengthPackage)
print(counterSendPackages)
print('Количество пакетов', maxCountPackages)

# print(dataToAis)
print(Fore.GREEN+'Данные получены'+Style.RESET_ALL)


# Функция, которая берет подстроку ответа от клиента и парсит ее
def parseAisResp(strAis):
    """
    Функция предназначена, чтобы взять строки ответа клиента подстроку формата JSON и пропарсить ее
    Пример:
    b'\x00\x00\x00\xab{"id":"30e3a9ea-36f9-4d0c-b215-7109df567bcb","command":"2898212c-870e-44b0-adb8-8a1a845a1f70","flags":2164267073,"content":{"password":"bf5343bbe49241e9ba54cacf61547e93"}}'
    Результат:
        ТИП ОБЪЕКТ
        {"id":"30e3a9ea-36f9-4d0c-b215-7109df567bcb","command":"2898212c-870e-44b0-adb8-8a1a845a1f70","flags":2164267073,"content":{"password":"bf5343bbe49241e9ba54cacf61547e93"}}
    КОММЕНТАРИИ:
    \x00\x00\x00\xab - длина строки ответа клиента (4 байта)
    """
    if (strAis.count("\\x00\\x00\\x00q")>1):
        # Костыль, т.к. приходит двойное ЭХО
        print(Fore.RED + 'КРИТИЧЕСКАЯ ОШИБКА!' + Style.RESET_ALL)
        correctEcho = strAis.split("\\x00\\x00\\x00q")
        print(Fore.YELLOW + 'Должно быть последним ЭХО на которое нужно ответить' + Style.RESET_ALL, correctEcho[-1][0:-1])
        obj = json.loads(correctEcho[-1][0:-1])
    else:
        obj = json.loads(strAis[strAis.find("{"):-1])
    return obj


def sendServToAis(strServJson, pr=None):
    """
    команда преобразует строку в байтовый формат и отправляет в формате длина сообщения + сообщение
    pr - признак, печатать ответное сообщение или нет
    share - признак делить сообщение или нет
    """

    strToAis = json.dumps(strServJson)
    length = len(strToAis)
    client_sock.send(length.to_bytes(4, 'big'))
    client_sock.send(strToAis.encode('UTF-8'))
    if (pr == None):
        print(Fore.GREEN+'Отправляем сообщение клиенту:'+Style.RESET_ALL)
        print(Style.BRIGHT+Fore.CYAN,
                  str(length.to_bytes(4, 'big'))+strToAis, Style.RESET_ALL)
    if (pr == 1):
        print(Fore.GREEN+f'Отправили {lengthPackage} записей клиенту:'+Style.RESET_ALL)


print(u'Создаем серверный соккет')
# AF_INET - семейство протоколов 'интернет' (INET)
# SOCK_STREAM - тип передачи данных 'потоковый' (TCP)
# proto - выбираем протокол 'по умолчанию' для TCP, т.е. IP
# создали серверный соккет, объект
server_sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM, proto=0)
print(type(server_sock))
print(server_sock.fileno())  # файловый дескриптор
# На этом порту сервер будет ожидать подключения клиентов '192.168.1.204' - это адресс локальной машины на котором создается серверный соккет
server_sock.bind(('192.168.1.204', 62065))
# 10 - это размер очереди входящих подключений, т.н. backlog
server_sock.listen(10)
"""
СОЕДИНЕНИЕ УСТАНАВЛИВАЕТСЯ -> УСТАНОВЛЕНО -> CОЕДИНЕНИЕ ЗАКРЫВАЕТСЯ
serv_sock.listen(10) - Эта команда говорит, что установленных соединений может быть не более 10
"""
count = 0  # Счетчик этераций в цикле вайл
while True:
    """
    В случае если происходит попытка соединения, то в client_sock записывается клиентский соккет, 
    а в client_addr записывается адресс клиента 
    """
    print(Fore.BLUE+'Клиент постучался'+Style.RESET_ALL)
    print(Fore.YELLOW + 'Ожидаю подключения соккета' + Style.RESET_ALL)
    # Вот здесь заглушка, которая ждет, когда начнут стучать.
    client_sock, client_addr = server_sock.accept()
    print(Fore.GREEN + 'Connected by' + Style.RESET_ALL, client_addr)

    while True:
        print(Back.GREEN, count, Style.RESET_ALL)
        # recv() – читает данные из сокета клиента. Аргумент устанавливает максимальное количество байтов в сообщении.
        data = client_sock.recv(2024)
        if (count == 0):
            print(Fore.YELLOW + 'UUID от клиента для проверки TCP соединения:\n' +
                  Style.RESET_ALL, Fore.CYAN, data, Style.RESET_ALL)
            print(Fore.GREEN + 'Ответ от сервера (тоже самое):\n' +
                  Style.RESET_ALL, Fore.CYAN, data, Style.RESET_ALL)
            client_sock.send(data)
            count = count+1
            continue
        elif (count == 1):
            print(Fore.YELLOW + 'ProtocolCompatible - запрос информации о совместимости от клиента:\n' +
                  Style.RESET_ALL, Fore.CYAN, data, Style.RESET_ALL)
            client_sock.send(data)
            print(Fore.GREEN + 'Отправляем клиенту в ответ тоже самое:\n' +
                  Style.RESET_ALL, Fore.CYAN, data, Style.RESET_ALL)
            count = count+1
        elif (count == 2):
            strAis = str(data)
            print(Fore.YELLOW + 'Сообщение от клиента об авторизации:\n' +
                  Style.RESET_ALL, Fore.CYAN, data, Style.RESET_ALL)
            fromClient = parseAisResp(strAis)
            print('Смена флаг сообщения клиента на флаг со статусом успешно')
            fromClient['flags'] = 2164267082  # Флаг статуса успешно
            sendServToAis(fromClient)
            count = count+1
        else:
            strAis = str(data)
            print(Fore.GREEN,datetime.datetime.today().strftime('%H:%M:%S'), Style.RESET_ALL, 'Смотрим, что приходит:', strAis, '\n')
            fromClient = parseAisResp(strAis)
            if (fromClient['command'] == ECHO):
                # Если Echo то послать Echo в ответ
                print(Fore.YELLOW+'ECHO от клиента:\n' +
                      Style.RESET_ALL, Fore.CYAN, data, Style.RESET_ALL)
                # print(Style.BRIGHT+Fore.GREEN+'Синхронное ECHO от сервера:\n' +
                #       Style.RESET_ALL, Fore.CYAN, data, Style.RESET_ALL)
                # client_sock.send(data)
                sendServToAis(fromClient)
                count = count+1
                continue
            elif (fromClient['command'] == GET_SYNC_DATA_CHECK):
                # Команда контрольных сумм SyncDataCheck
                print(Fore.YELLOW+'Команда от клиента SyncDataCheck:\n' +
                      Style.RESET_ALL, Fore.CYAN, data, Style.RESET_ALL)
                fromClient['flags'] = 2164263050
                fromClient['content']['crc'] = crc
                sendServToAis(fromClient)
                count = count+1
            elif (fromClient['command'] == SYNC_DATA_COUNT):
                # Команда SyncDataCount
                fromClient['flags'] = 2164263050
                sendServToAis(fromClient)
                count = count+1
            else:
                if (fromClient['command'] == GET_SYNC_DATA and fromClient['content']['count'] == 1):
                    # Если просит прислать данные GetSyncData то сначала присылает 1 запись
                    print(Fore.YELLOW+'Формат данных, которые ожидает клиент:\n' +
                          Style.RESET_ALL, Fore.MAGENTA, data, Style.RESET_ALL)
                    fromClient['content']['items'] = ora.getDataToAis(0, year, month, 1, 1)
                    fromClient['content']['count'] = 1
                    fromClient['flags'] = 2164263050
                    fromClient['content']['period']['begin'] = int(datetime.datetime(
                        hf.setYear(year, month), hf.lastMonth(month), 1).timestamp())
                    fromClient['content']['period']['end'] = int(datetime.datetime(hf.setYear(
                        year, month), hf.lastMonth(month), hf.setDay(year, month)).timestamp())
                    sendServToAis(fromClient)
                    count = count+1
                    continue
                    # Вернуть на 5000 тысяч если нужно
                if (fromClient['command'] == GET_SYNC_DATA and counterSendPackages < maxCountPackages and fromClient['content']['count'] != 1):
                    # Если просит прислать 5000 тысяч то шлем 5000 тысяч записей
                    print(Fore.YELLOW+'Формат данных, которые ожидает клиент:\n' +
                          Style.RESET_ALL, Fore.MAGENTA, data, Style.RESET_ALL)
                    fromClient['content']['items'] = ora.getDataToAis(0, year, month, 1+counterSendPackages*lengthPackage, (counterSendPackages+1)*lengthPackage) #dataToAis5000[counterSendPackages*lengthPackage:(counterSendPackages+1)*lengthPackage]
                    fromClient['flags'] = 2164263050
                    fromClient['content']['period']['begin'] = int(datetime.datetime(
                        hf.setYear(year, month), hf.lastMonth(month), 1).timestamp())
                    fromClient['content']['period']['end'] = int(datetime.datetime(hf.setYear(
                        year, month), hf.lastMonth(month), hf.setDay(year, month)).timestamp())
                    sendServToAis(fromClient, 1)
                    count = count+1
                    counterSendPackages = counterSendPackages + 1
                    print(f'{counterSendPackages} пакетов отправлено')
                    continue
                if (fromClient['command'] == GET_SYNC_DATA and counterSendPackages == maxCountPackages):
                    # Необходимо как-то сказать клиенту, что мы более не будем посылать данные, поэтому посылаем пустое сообщение.
                    print(
                        Fore.YELLOW+'Посылаем пустое значение в ответ, чтобы закончить синхронизацию'+Style.RESET_ALL)
                    fromClient['flags'] = 2164263050
                    sendServToAis(fromClient)
                    count = count+1
                    continue

            if not data:
                print('Not Data')
                break
    client_sock.close()

"""
Методы для формирования таблицы FOND.AIS_MODELS с подготовленынми данными 
"""
import cx_Oracle
import datetime
import json
def db_connect():
    """
    Функция для подключения к базе данных TФОМС-Оракл
    """
    userpwd='пароль свой'
    connection = cx_Oracle.connect("FOND", userpwd, "указывается свой", encoding="UTF-8")
    cursor = connection.cursor()
    return cursor, connection
def db_close(connection):
    """
    Функция для закрытия соединения
    """
    from colorama import init, Fore, Back, Style
    connection.close()
    print(Fore.YELLOW + 'Соединение закрыто!' + Style.RESET_ALL)

def normDate(x):
    """
    Приведение времени к требуемому формату
    """
    if type(x)==datetime.datetime:
        # x=x.strftime('%d.%m.%Y.%f')
        x=x.strftime('%d.%m.%Y')
    return x
def ora_fetch_assoc(cursor):
    """
    Пишем данные в формате фетч
    """
    b=datetime.datetime.timestamp(datetime.datetime.today())
    res = []
    try:
        desk = [d[0] for d in cursor.description]
        res = [dict(zip(desk,map(normDate,line))) for line in cursor]
        print(res.__sizeof__())
    except cx_Oracle.DatabaseError as e:
        res = e
    e=datetime.datetime.timestamp(datetime.datetime.today())
    t = int(e-b)
    print(f'Время обработки данных курсора в массив {t}')
    return res
    
def testPrint():
    print('Библиотека для работы с БД подключена')

# Получаем контрольную сумму по записям npp - порядковый номер записи
def getCheckSum(year, month, npp_min, npp_max):
    """
    Получение контрольной суммы за год year, месяц month, npp_min - с какой, npp_max - по какую.
    """
    cursor, connection = db_connect()
    checksum = cursor.var(cx_Oracle.NUMBER)
    b=datetime.datetime.timestamp(datetime.datetime.today())
    ret = cursor.callproc('FOND.PKG_AIS.GET_CHECKSUM', [year, month, npp_min, npp_max, checksum])
    e=datetime.datetime.timestamp(datetime.datetime.today())
    timeFunction = int(e-b)
    print(f'Время выполнения процедуры по возврату контрольной суммы: {timeFunction} cек')
    db_close(connection)
    return int(ret[4])

def getRecCount(year, month):
    """
    Получение количества записей за year - год, month - месяц.
    """
    cursor, connection = db_connect()
    recCount = cursor.var(cx_Oracle.NUMBER)
    b=datetime.datetime.timestamp(datetime.datetime.today())
    ret = cursor.callproc('FOND.PKG_AIS.GET_RECCOUNT', [year, month, recCount])
    e=datetime.datetime.timestamp(datetime.datetime.today())
    timeFunction = int(e-b)
    print(f'Время выполнения процедуры по возврату количества записей: {timeFunction} сек')
    db_close(connection)
    return int(ret[2])

def getDataToAis(refill, year,month, npp_min, npp_max):
    """
    #refill: 0 - звять старые, 1 - обновить
    #year: год
    #month: анализируемый период
    #mkod: код больницы
    #recCount: если записей много, вернуть то количество, которое указано, если параметр очень большой, то вернет все
    """
    res = 'Данных нет'
    resSum = 'Контрольная сумма равна нулю'
     
    cursor, connection = db_connect()
    outVal = cursor.var(cx_Oracle.CURSOR)
    b=datetime.datetime.timestamp(datetime.datetime.today())
    ret = cursor.callproc('FOND.PKG_AIS.GET_RST_TO_AIS', [refill, year, month, npp_min, npp_max, outVal])
    e = datetime.datetime.timestamp(datetime.datetime.today())
    print(f'Время выполнения процедуры в Oracle: {int((e-b))} сек')
    if (refill == 0):
        res = ora_fetch_assoc(ret[5])
        print('Размер данных в отправляемом массиве в байтах: ',res.__sizeof__())
    db_close(connection)
    return res

if __name__ == '__main__':
    d = datetime.datetime.today()
    print('Начало выполнения программы: ',d.strftime('%H:%M:%S'))

    rC = getRecCount(2020, 4)
    # chSum = getCheckSum(2019, 8, 1, 500000)
    # print('Число записей', rC, 'Контрольная сумма', chSum)
    print('Число записей', rC)
    getDataToAis(1, 2020, 4, None, None)
    # print(getDataToAis(0, 2019, 8, 1, 1))
    d = datetime.datetime.today()
    print('Конец выполнения программы: ',d.strftime('%H:%M:%S'))
    # print(getDataToAis(0, 2019,12,810126)[1])

    """
    Если нужно залить новые записи в таблицу за новый период необходимо выполнить 
    метод getDataToAis(1, 2020, 2, None, None)
    """ 
     
           



CREATE OR REPLACE PACKAGE FOND.PKG_AIS AS
/******************************************************************************
   NAME:       PKG_AIS
   PURPOSE:

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        20/02/2020      lev_sv       1. Created this package.
******************************************************************************/

PROCEDURE GET_RST_TO_AIS(
    REFILL#     IN INT,
    YEAR#       IN INT,
    MONTH#      IN INT,
    NPP_MIN#    IN INT,     -- ������� � ������ �����
    NPP_MAX#    IN INT,     -- ���������� ������� �����
    RET$        OUT TYPES.RetCursor
);

--FUNCTION GET_CRC(ID_PERIOD# IN INT)
--RETURN NUMBER;

FUNCTION DATE_TO_INT(DATE# IN DATE)
RETURN INT;

PROCEDURE GET_CHECKSUM(
    YEAR#       IN INT,     -- �������� ���
    MONTH#      IN INT,     -- �������� �����
    NPP_MIN#    IN INT,     -- ������� � ������ �����
    NPP_MAX#    IN INT,     -- ���������� ������� �����
    CHECKSUM$   OUT INT    -- ����������� �����
);

PROCEDURE GET_RECCOUNT(
    YEAR#       IN INT,     -- �������� ���
    MONTH#      IN INT,     -- �������� �����
    RECCOUNT$   OUT INT    -- ���������� �������
);

PROCEDURE GET_RST_WITH_SCORE(
    MENU#       IN INT,             -- ��� ���� ������
    ID_USER#    IN INT,             -- ��� ������������ (WEB_USERS.ID)
    ID_PERIOD#  IN INT,             -- ������������� ��������� ������� (F2014.PERIOD.ID)
    USL_OK#     IN INT,             -- ��� ������� �������� ������ (SPR.SPFF_V006.IDUMP)
    LPU#        IN VARCHAR2,        -- ��� �� (SPR.SP_LPU.MKOD)
    SMK#        IN VARCHAR2,        -- ��� ��� (SPR.SP_SMO.SMOCOD)
    LIST_DS#    IN VARCHAR2,        -- ������ ����� ���������, ����� ����������� (�������)
    TYPE_EXP#   IN INT,             -- ��� ����������: 2-���, 3-����
    SCORE_MIN#  IN INT,             -- ������ ������� ������
    SCORE_MAX#  IN INT,             -- ������� ������� ������
    RET_MSG$   OUT VARCHAR2,        -- ��������� ���������
    RET_CUR$   OUT TYPES.RETCURSOR  -- �������������� ������
);
END PKG_AIS;
/
# Развертывание системы на Linux

**Установка базы данных Firebird 3:**

1. В процессе установки БД потребуется установить библиотеку **libtommath-dev** командой *sudo apt-get install libtommath-dev*  
2. Необходимо создать символьную ссылку *ln -sf /usr/lib/x86_64-linux-gnu/libtommath.so.1 /usr/lib/x86_64-linux-gnu/libtommath.so.0*  
3. Скачать БД командой *wget https://github.com/FirebirdSQL/firebird/releases/download/R3_0_4/Firebird-3.0.4.33054-0.amd64.tar.gz*  
4. Произвести установку командой *sudo bash install.sh*  
5. Выполнить перезагрузку *sudo reboot*  
6. После перезагрузки убедиться, что служба файрберд запустилась на нужном порту *sudo netstat -ntlp* 

**Установка aisexpert:**

1. Скачиваем свежую версию sudo *dpkg -i aisexpert-web-0.9.5-20191203gitd7bfe09-ubuntu18.04-amd64.deb*  
2. Устанавливаем *sudo apt-get install -f*  
3. Переименовываем aisexpert-empty.fdb d aisexpert.fdb  


# Web интерфейс  

IP адресс виртуального сервера - 192.168.30.20  
Логин - User1  
Password - 111  


# Работа с терминалом Linux

Открыть терминал - ctr+alt+T
Для отслеживания всех процессов синхронизации сервера с клиентом необходимо перейти в дирректорию */var/opt/aisexpert/log*  
Команда *tail -f aisexpert.log.syncplan* будет непрерывно выводить в терминал логи синхронизации данных, а именно отображать информацию о том, сколько записей положено во временную таблицу.

[Пример логов](https://bitbucket.org/chessplanet86/ais_expert_connector/src/master/example_log.png) 

Команда *tail -f aisexpert.log.transport* детально показывает все ошибки.
Команда *sudo mcedit /etc/aisexpert/aisexpert.conf* открывает конфигурации с правами администратора для возможного редактирования.  
Команда перезагрузки службы aisexpert *sudo systemctl restart aisexpert*.  
Команда для просмотра общих логово *tail -f aisexpert.log*.  
Файл с правильными конфигурациями для открытия всех нужных логов находится [здесь](https://bitbucket.org/chessplanet86/ais_expert_connector/src/master/aisexpert.logger.conf)


# Алгоритм формирования данных для построения моделей в БД Oracle

1. После загрузки реестров необходимо залить новые данные в таблицу **FOND.AIS_MODELS**. Необходимо открыть файл [oracleConnect.py](https://bitbucket.org/chessplanet86/ais_expert_connector/src/master/oracleConnect.py) 
2. В разделе после `if __name__ == '__main__':` должен присутствовать метод `getDataToAis(refill, year,month, npp_min, npp_max)`. Первый мараметр в значении `1` записывает данные в таблицу, в значении `0` данные будут извлекаться. Второй параметр год. Третий параметр месяц. Последние два параметра нужны для извлечения данных (в состоянии записы должны быть None). Третий параметр казывает с какой строки таблицы данные будут извлекаться. Четвертый параметр указывает какой строкой извлечение данных будет закончено. Для проверки количества записей за определенный период можно использовать метод `getRecCount(year, month)`. Для этого метод `getDataToAis` должен быть закомментирован, в противном случае код будет выполняться очень долго.
3. Далее необходимо удалить из таблицы FOND.AIS_MODELS дублирующиеся записи скриптом 1 SQL. Не забыдь сделать коммит.
4. После этого записи нужно переименовать, т.к. порядок записей в таблице мог сбиться. Необходимо выполнить скрипт 2 SQL.  
5. После загрузки в таблицу FOND.AIS_MODELS необходимо залить данные в AIS. Для этого в [inVal.py](https://bitbucket.org/chessplanet86/ais_expert_connector/src/master/inVal.py) необходимо указать параметры `month`,
`year`, остальные параметры не трограть. Параметр `lengthPackage` указывается в зависимости от конфигурационных настроек [aisexpert.logger.conf](https://bitbucket.org/chessplanet86/ais_expert_connector/src/master/aisexpert.logger.conf)  
```
--SQL Script1
DELETE FROM AIS_MODELS  
WHERE GKEY IN (SELECT T.GKEY FROM AIS_MODELS T  
               GROUP BY T.GKEY 
               HAVING COUNT(*) > 1)
```
```
--SQL Script2 
DECLARE   
    C INT:=0;  
BEGIN  
FOR R IN (  
    SELECT ID, ID_PERIOD, ROW_NUMBER() OVER (PARTITION BY ID_PERIOD ORDER BY ID_PERIOD, TIME_MARK) AS NPP  
        FROM AIS_MODELS  
) LOOP  
    UPDATE AIS_MODELS SET NPP = R.NPP WHERE ID = R.ID;  
    IF C > 1000 THEN  
        C:=0;  
        COMMIT;  
    END IF;  
    C:=C+1;  
END LOOP;  
COMMIT;  
END;  
```


# Алгоритм формирования данных для построения моделей в БД Firebird  
1. Необходимо в браузере перейти http://192.168.30.20/learn.  
2. В разделе __Обучение__ создать новую задачу по [примеру](https://bitbucket.org/chessplanet86/ais_expert_connector/src/master/data_fill_web0.png).  
Должна появиться [запись](https://bitbucket.org/chessplanet86/ais_expert_connector/src/master/data_fill_web1.png). 
3. Отключить регулярную задачу обучения.
4. Запустить скрипт `serverSocket_1.1.py` через интерпритатор питон. Программа подсчитает, какое количество пакетов должно быть отправлено. Должен выполниться ProtocolCompatible. Должна произойти авторизация. Будут выведены сообщения об отправленных пакетах.  
5. В случае, если возникают ошибки с заливкой, то причины могут скрываться в неправильном формате данных а таблице **FOND.AIS_MODELS**. Для устранения ошибок необходимо проанализировать файлы логов *aisexpert.log.syncplan*, *aisexpert.log*, *aisexpert.log.transport*.  
6. После окончания заливки данных необходимо прервать выполнение заливки нажав на [значок](https://bitbucket.org/chessplanet86/ais_expert_connector/src/master/stop.PNG).


# Обработка залитых данных в БД Firebird  
1. Для работы с Firebird Необходимо скачать IBExpert. Только этот менеджер позволяет корректно работать с данной БД.  
2. Хост подключеия __192.168.30.20__. Порт __3050__  
3. Указать путь до самой БД __/var/opt/aisexpert/database/aisexpert.fdb__  
4. Имя пользователя __SYSDBA__. Пароль __masterkey__.  
5. Необходимо в таблице SYNC_DATA полям MKB2, MKB3 и country присвоить значения null. Не забыдь подтердить транзакцию.  


# Применение  
1. Необходимо перейти в раздел применение и создать задачу применения.  
2. Необходимо заполнить форму согласно [примеру](https://bitbucket.org/chessplanet86/ais_expert_connector/src/master/example_application.PNG). Должен запуститься процесс выполнения. Если progress bar преодолел отметку в 1%, то с большой вероятностью выполнение зхавершится успешно.  
 

# Выгрузка данных в нашу БД  
1. После того, как выгрузка завершена, в разделе применение ниже в [оценках](https://bitbucket.org/chessplanet86/ais_expert_connector/src/master/Score_web_example.PNG) появится оценка за указанный период.  
2. Необходимо кликнуть по значку карандаша, далее кликнуть по ссылке *Показать техническую информацию*.  
3. Скопировать идентификатор. Записи с построенной оценкой находятся в БД Firebird в таблице __SCORE_DATA__ и соответствуют данному идентификатору.  
4. Для копирования всех записей необходимо открыть [firebirdConnect.py](https://bitbucket.org/chessplanet86/ais_expert_connector/src/master/firebirdConnect.py) и вставить идентификатор в правильном формате в соответствующее место кода.  
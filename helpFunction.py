def lastMonth(month):
    assert 0<month<13, 'Не правильно указан месяц'
    if (month==1):
        return 12
    else:
        return month-1

def setYear(year, month):
    assert type(year)==int, 'тип года не int'
    assert 0<month<13, 'Не правильно указан месяц'
    if (month==1):
        return year-1
    else:
        return year
def setDay(year, month):
    vis = False
    # print(f'Входной месяц: {month}')
    # print(f'Входной год: {year}')
    lmonth = lastMonth(month)
    y = setYear(year, month)
    if y % 4 != 0 or (y % 100 == 0 and y % 400 != 0):
        pass
    else:
        vis = True
    # print(f'Год на выходе: {lmonth}')
    # print(f'Месяц на выходе: {y}')
    if (lmonth in (1,3,5,7,8,10,12)):
        day = 31
        # print(f'Количество дней в месяце: {day}')
        return day
    if (lmonth in (4,6,9,11)):
        day = 30
        # print(f'Количество дней в месяце: {day}')
        return day
    if (lmonth ==2):
        if (vis!=True):
            day = 28
            # print(f'Количество дней в месяце: {day}')
            return day
        else:
            day = 29
            # print(f'Количество дней в месяце: {day}')
            return day 






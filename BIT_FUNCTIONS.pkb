CREATE OR REPLACE PACKAGE BODY FOND.bit_functions as 
   function num_to_bits (in_val in number) return varchar2
   is 
      work_1 number; 
      work_2 number; 
      work_3 number; 
      digit  varchar2(1); 
      accum  varchar2(64); 
   begin 
      accum  := ''; 
      work_1 := in_val; 
      work_2 := in_val; 
      loop 
         work_2 := work_1/2; 
         work_3 := trunc(work_2); 
         if work_2 = work_3 then digit := '0'; 
         else digit := '1'; end if; 
         accum := digit || accum; 
         if work_3 = 0 then exit; end if; 
         work_1 := work_3; 
      end loop; 
      return accum; 
   end; 
   function bits_to_num (in_val in varchar2) return number
   is 
      i     number; 
      digit number; 
      accum number; 
   begin 
      accum := 0; 
      for i in 1..length(in_val) loop 
         digit := to_number(substr(in_val,i,1)); 
         accum := 2*accum + digit; 
      end loop; 
      return accum; 
   end; 
   function bit_and (in_1 in number, in_2 in number) return number
   is 
      work_1 varchar2(64); 
      work_2 varchar2(64); 
      digit  number; 
      i      number; 
      accum  number; 
   begin 
      work_1 := num_to_bits (in_1); 
      work_2 := num_to_bits (in_2); 
      accum  := 0; 
      if length(work_1) > length(work_2) then 
         work_2 := lpad(work_2,length(work_1),'0'); 
      end if; 
      if length(work_2) > length(work_1) then 
         work_1 := lpad(work_1,length(work_2),'0'); 
      end if; 
      for i in 1..length(work_1) loop 
         if substr(work_1,i,1) = '1' and substr(work_2,i,1) = '1' then 
            digit := 1; 
         else 
            digit := 0; 
         end if; 
         accum := 2*accum + digit; 
      end loop; 
      return accum; 
   end; 
   function bit_or (in_1 in number, in_2 in number) return number 
   is 
      work_1 varchar2(64); 
      work_2 varchar2(64); 
      digit  number; 
      i      number; 
      accum  number; 
   begin 
      work_1 := num_to_bits (in_1); 
      work_2 := num_to_bits (in_2); 
      accum  := 0; 
      if length(work_1) > length(work_2) then 
         work_2 := lpad(work_2,length(work_1),'0'); 
      end if; 
      if length(work_2) > length(work_1) then 
         work_1 := lpad(work_1,length(work_2),'0'); 
      end if; 
      for i in 1..length(work_1) loop 
         if substr(work_1,i,1) = '1' or substr(work_2,i,1) = '1' then 
            digit := 1; 
         else 
            digit := 0; 
         end if; 
         accum := 2*accum + digit; 
      end loop; 
      return accum; 
   end; 
   function bit_xor (in_1 in number, in_2 in number) return number 
   is 
      work_1 varchar2(64); 
      work_2 varchar2(64); 
      digit  number; 
      i      number; 
      accum  number; 
   begin 
      work_1 := num_to_bits(in_1); 
      work_2 := num_to_bits(in_2); 
      accum  := 0; 
      if length(work_1) > length(work_2) then 
         work_2 := lpad(work_2,length(work_1),'0'); 
      end if; 
      if length(work_2) > length(work_1) then 
         work_1 := lpad(work_1,length(work_2),'0'); 
      end if; 
      for i in 1..length(work_1) loop 
         if substr(work_1,i,1) = substr(work_2,i,1) then 
            digit := 0; 
         else 
            digit := 1; 
         end if; 
         accum := 2*accum + digit; 
      end loop; 
      return accum; 
   end; 
   function bit_lsh (in_num in number, in_shft in number default 1)
   return number 
   is 
   begin 
      return power(2,in_shft)*in_num; 
   end; 
   function bit_rsh (in_num in number, in_shft in number default 1)
   return number 
   is 
   begin 
      return trunc(in_num/power(2,in_shft)); 
   end; 
end;
/
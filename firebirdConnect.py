import fdb # модуль для работы с файберд
import cx_Oracle #модуль для работы с Oracle
import datetime
# Подключаемся к базе данных файрберд
con = fdb.connect(dsn='192.168.30.20:/var/opt/aisexpert/database/aisexpert.fdb', user='SYSDBA', password='masterkey')
# Подключаемся к базе данных Oracle
userpwd='startup'
connection = cx_Oracle.connect("FOND", userpwd, "192.168.1.234:1521/ORCL", encoding="UTF-8")
oracleCursor = connection.cursor()
print('Объект соединения: ', con)
firebirdCursor = con.cursor()
res = firebirdCursor.execute("""
SELECT UUID_TO_CHAR(score_id), UUID_TO_CHAR(GKEY), CODE_LPU, MKB1, PROFIL, VID_MP, ROUND(MEE,3)*100 MEE, ROUND(EKMP, 3)*100 EKMP
FROM SCORE_DATA 
WHERE UUID_TO_CHAR(score_id)='C945AC9D-E48A-4460-BE53-9A3F84E82662' -- Вставить код модели
""")
print('Выборка сделана')
# for (line, i) in zip(res, range(10)):
#     print(line)
count = 0
for line in res:
    count=count+1
    oracleCursor.execute("INSERT into AIS_SCORE_DATA values (:score_id, :gkey, :code_lpu, :mkb1, :profil, :vid_mp, :id_period, :count_month, :mee, :ekmp)",
    [line[0], line[1],line[2], line[3], line[4], line[5], 148, 5, line[6], line[7]])
    if(count%100000 == 0):
        d = datetime.datetime.today()
        print(d.strftime('%H:%M:%S'), f'Загружено: {count} записей')
connection.commit()

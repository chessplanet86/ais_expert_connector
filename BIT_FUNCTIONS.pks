CREATE OR REPLACE PACKAGE FOND.bit_functions as 
   function num_to_bits (in_val in number)               return varchar2; 
   function bits_to_num (in_val in varchar2)             return number; 
   function bit_and     (in_1 in number, in_2 in number) return number; 
   function bit_or      (in_1 in number, in_2 in number) return number; 
   function bit_xor     (in_1 in number, in_2 in number) return number; 
   function bit_lsh     (in_num in number,
                         in_shft in number default 1)    return number; 
   function bit_rsh     (in_num in number,
                         in_shft in number default 1)    return number; 
   end;
/